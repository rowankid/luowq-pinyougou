package cn.luowq.pinyougou.sellergoods.service;

import java.util.List;

import cn.luowq.pinyougou.pojo.TbBrand;

public interface BrandService {

	public List<TbBrand> findAll();
	
}
